package ru.tsc.felofyanov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;
import ru.tsc.felofyanov.tm.dto.request.UserProfileRequest;
import ru.tsc.felofyanov.tm.dto.response.UserProfileResponse;
import ru.tsc.felofyanov.tm.enumerated.Role;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.exception.entity.UserNotFoundException;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getName() {
        return "user-view-profile";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "View profile of current user";
    }

    @Nullable
    @Override
    public Role @Nullable [] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[VIEW USER PROFILE]");
        @NotNull final UserProfileResponse response =
                getUserEndpoint().viewProfileUser(new UserProfileRequest(getToken()));
        @Nullable final UserDTO user = response.getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }
}
