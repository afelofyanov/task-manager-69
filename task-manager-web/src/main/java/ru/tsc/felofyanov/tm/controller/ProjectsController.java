package ru.tsc.felofyanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.felofyanov.tm.service.ProjectService;
import ru.tsc.felofyanov.tm.util.UserUtil;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectService service;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", service.findAll(UserUtil.getUserId()));
    }
}
