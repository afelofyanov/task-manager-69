package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @Transactional
    void create(@Nullable String userId);

    @Transactional
    Task save(@Nullable String userId, @Nullable Task task);

    @Transactional
    List<Task> save(@Nullable String userId, @Nullable List<Task> tasks);

    @Transactional
    Collection<Task> findAll(@Nullable String userId);

    @Transactional
    Task findFirstByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Transactional
    void removeByByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Transactional
    void removeByUserIdAndEntity(@Nullable String userId, @Nullable Task task);

    @Transactional
    void remove(@Nullable String userId, @Nullable List<Task> tasks);

    @Transactional
    boolean existsById(@Nullable String userId, @Nullable String id);

    @Transactional
    void clear(@Nullable String userId);

    @Transactional
    long count(@Nullable String userId);
}
