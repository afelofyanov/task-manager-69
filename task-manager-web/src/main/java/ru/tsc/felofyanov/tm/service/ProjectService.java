package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.api.service.IProjectService;
import ru.tsc.felofyanov.tm.exception.IdEmptyException;
import ru.tsc.felofyanov.tm.exception.ModelEmptyException;
import ru.tsc.felofyanov.tm.exception.UserIdEmptyException;
import ru.tsc.felofyanov.tm.model.Project;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @Override
    @Transactional
    public void create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Project project = new Project(
                userId,
                "New project: " + System.currentTimeMillis(),
                "Description " + System.currentTimeMillis());
        repository.save(project);
    }

    @Override
    @Transactional
    public Project save(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ModelEmptyException();
        project.setUserId(userId);
        return repository.save(project);
    }

    @Override
    @Transactional
    public List<Project> save(@Nullable final String userId, @Nullable final List<Project> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null) throw new ModelEmptyException();
        for (Project project : projects) {
            project.setUserId(userId);
            repository.save(project);
        }
        return projects;
    }

    @Override
    @Transactional
    public Collection<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public Project findFirstByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeByByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeByUserIdAndEntity(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ModelEmptyException();
        repository.deleteByUserIdAndId(userId, project.getId());
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final List<Project> projects) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projects == null) throw new ModelEmptyException();
        projects
                .stream()
                .forEach(project -> removeByUserIdAndEntity(userId, project));
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return repository.findFirstByUserIdAndId(userId, id) != null;
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }
}
