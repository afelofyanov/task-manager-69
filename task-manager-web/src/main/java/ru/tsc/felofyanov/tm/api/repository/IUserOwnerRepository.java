package ru.tsc.felofyanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.felofyanov.tm.model.AbstractWbs;

import java.util.List;

@NoRepositoryBean
public interface IUserOwnerRepository<M extends AbstractWbs> extends IRepository<M> {

    @NotNull
    List<M> findAllByUserId(@Nullable String userId);

    void deleteByUserId(@NotNull String userId);

    @Nullable
    M findFirstByUserIdAndId(@Nullable String userId, @Nullable String id);

    M deleteByUserIdAndId(@Nullable String userId, @Nullable String id);

    long countByUserId(@Nullable String userId);
}
