package ru.tsc.felofyanov.tm.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.tsc.felofyanov.tm.endpoint.AuthEndpoint;
import ru.tsc.felofyanov.tm.endpoint.ProjectEndpointImpl;
import ru.tsc.felofyanov.tm.endpoint.TaskEndpointImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@EnableWebMvc
@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
@ComponentScan("ru.tsc.felofyanov.tm")
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer, WebApplicationInitializer {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/ws/*").permitAll()

                .and()
                .authorizeRequests()

                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/auth")
                .loginPage("/login")
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .csrf().disable();
    }

    @Bean
    public ViewResolver internalResourceViewResolver() {
        @NotNull final InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public Endpoint authEndpointRegistry(@NotNull final AuthEndpoint authEndpoint, @NotNull final SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectEndpointRegistry(@NotNull final ProjectEndpointImpl projectEndpoint, @NotNull final SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(@NotNull final TaskEndpointImpl taskEndpoint, @NotNull final SpringBus cxf) {
        @NotNull final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Override
    public void onStartup(@NotNull final ServletContext servletContext) throws ServletException {
        @NotNull final CXFServlet cxfServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dynamic = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamic.addMapping("/ws/*");
        dynamic.setLoadOnStartup(1);
    }
}