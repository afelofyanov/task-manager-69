package ru.tsc.felofyanov.tm.exception;

import org.springframework.security.access.AccessDeniedException;

public class AccessException extends AccessDeniedException {

    public AccessException() {
        super("Error! Access denied...");
    }
}
