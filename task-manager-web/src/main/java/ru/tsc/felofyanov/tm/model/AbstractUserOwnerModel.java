package ru.tsc.felofyanov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnerModel extends AbstractModel {

    @Nullable
    @Column(name = "user_id")
    private String userId;

    public AbstractUserOwnerModel(@Nullable final String userId) {
        this.userId = userId;
    }
}
