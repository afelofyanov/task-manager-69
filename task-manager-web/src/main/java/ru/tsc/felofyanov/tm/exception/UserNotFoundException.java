package ru.tsc.felofyanov.tm.exception;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }
}
