package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.enumerated.RoleType;
import ru.tsc.felofyanov.tm.model.User;

public interface IUserService {

    @Transactional
    void createUser(@Nullable String login, @Nullable String password, @Nullable RoleType roleType);

    @Nullable
    @Transactional
    User findByLogin(@Nullable String login);
}
