package ru.tsc.felofyanov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable ProjectDTO project) {
        super(project);
    }
}
