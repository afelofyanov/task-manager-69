package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.SessionDTO;
import ru.tsc.felofyanov.tm.dto.model.UserDTO;

public interface IAuthService {

    @Nullable
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    SessionDTO validateToken(@NotNull String token);

    @NotNull
    String login(@Nullable String login, @Nullable String password);
}
