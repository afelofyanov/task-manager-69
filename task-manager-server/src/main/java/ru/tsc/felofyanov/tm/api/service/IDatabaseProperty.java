package ru.tsc.felofyanov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUserName();

    @NotNull
    String getDatabaseUserPassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseFormatSql();

    @NotNull
    String getDatabaseUseSecondL2Cache();

    @NotNull
    String getDatabaseUseQueryCache();

    @NotNull
    String getDatabaseUseMinimalPuts();

    @NotNull
    String getDatabaseRegionPrefix();

    @NotNull
    String getDatabaseProviderConfigurationFileResourcePath();

    @NotNull
    String getDatabaseRegionFactoryClass();
}
