package ru.tsc.felofyanov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IServiceDTO<M extends AbstractModelDTO> {

    M add(M model);

    Collection<M> add(@Nullable Collection<M> models);

    M update(M model);

    @NotNull List<M> findAll();

    boolean existsById(@Nullable String id);

    @NotNull Collection<M> set(@NotNull Collection<M> models);

    void clear();

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    M remove(M model);

    M removeById(@Nullable String id);

    M removeByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<M> collection);

    long count();
}
